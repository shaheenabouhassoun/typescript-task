this project is doing all the requested of the first task :
#A webpage or a command line app written in TypeScript that:
1-Shows the last block number of Ethereum mainnet.
2-Shows the USDT balance of a provided address.
3-Contains a unit test.
4-use TypeScript Generics at some part of the code.

this project is done using the angular framework, So you can start the project by running the command (ng s --o) , and for the tests you can use the (ng test) command .