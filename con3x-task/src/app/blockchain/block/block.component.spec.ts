import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { BlockComponent } from './block.component';
import { By } from '@angular/platform-browser';

describe('BlockComponent', () => {
  let block: BlockComponent;
  let fixture: ComponentFixture<BlockComponent>;
  let el: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BlockComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    });
    fixture = TestBed.createComponent(BlockComponent);
    el = fixture.debugElement;
    block = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(block).toBeTruthy();
  });

  it('should render 4 h1 tags and 2 buttons', () => {
    const h1Elements = el.queryAll(By.css('h1'));
    const btnElements = el.queryAll(By.css('.btn'));
    expect(h1Elements[0].nativeElement.textContent).toBe('Last Blockchain Number :');
    expect(h1Elements[1].nativeElement.textContent).toBe('0');
    expect(h1Elements[2].nativeElement.textContent).toBe('Balance of the given USDT contract :');
    expect(h1Elements[3].nativeElement.textContent).toBe('0');
    expect(btnElements[0].nativeElement.textContent).toBe('show last block');
    expect(btnElements[1].nativeElement.textContent).toBe('show Balance');
  });

  it('should call the suitable function for each button after clicking on them', () =>{
    const btnElements = el.queryAll(By.css('.btn'));
    spyOn(block,"getLastBlockNum");
    btnElements[0].nativeElement.click();
    fixture.detectChanges();
    expect(block.getLastBlockNum).toHaveBeenCalled();
    spyOn(block,"getBalanceOfUSDT");
    btnElements[1].nativeElement.click();
    fixture.detectChanges();
    expect(block.getBalanceOfUSDT).toHaveBeenCalled();
  })
  
});
